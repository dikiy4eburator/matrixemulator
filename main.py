# PrimeArch  Copyright (C) 2020  Wladimir Frank

from time import sleep
from threading import Thread
from queue import Queue
from subprocess import call

from e_ascii import grafik_engine
from rain import let_it_rain

def input_loop(engine, return_Q, kill_chan):

    while True:
        if not return_Q.empty():
            tmp = return_Q.get()
            engine.FB = tmp

        # INPUT BLOCK
        if not engine.key_buffer.empty():
            input_main_loop = engine.key_buffer.get()

            if input_main_loop == "KEY_ESCAPE":
                kill_chan.put("kill")
                sleep(0.2)
                engine.main_loop_run = False
                break

        sleep(0.01)


if __name__ == "__main__":

    #print("\n" * 50)

    # create grafik engine objekt
    engine = grafik_engine()

    # Prepare and start rain processing
    kill_chan = Queue()
    return_Q = Queue()

    rain_processing = Thread(target=let_it_rain, args=(return_Q, kill_chan,))
    rain_processing.start()

    # start input/control loop
    input_thread = Thread(target=input_loop, args=(engine, return_Q, kill_chan,))
    input_thread.start()

    # start engine main loop
    engine.main_loop()

    call("tput init".split(" "))
    call("tput clear".split(" "))
