from e_ascii import draw_char
from random import randrange, choice



class rain_drop:

    def __init__(self, engine, my_drawer_index):

        #self.jap = self.getchars(19968, 19978)
        #self.chars = self.jap

        # create char table
        self.latin = self.getchars(0x30, 0x80)
        self.greek = self.getchars(0x390, 0x3d0)
        self.hebrew = self.getchars(0x5d0, 0x5eb)
        self.cyrillic = self.getchars(0x400, 0x50)
        self.chars = self.hebrew + self.greek + self.latin + self.cyrillic


        self.engine = engine
        self.my_drawer_index = my_drawer_index
        # speed in raws per second
        self.speed = self.get_new_speed()
        self.trail_lengh = self.get_new_trail()
        self.fps = engine.FPS
        # cykles per raw
        self.cycles_per_raw = self.speed * self.fps

        self.yelow = engine.color_conv.rgb_to_color("f", 0, 255, 255, 0)
        self.orange = engine.color_conv.rgb_to_color("f", 0, 196, 127, 44)
        self.col_brite = engine.color_conv.rgb_to_color("f", 1, 116, 179, 0)
        self.col_background = engine.color_conv.rgb_to_color("f", 0, int(116 * 0.3), int(179 * 0.3), int(0 * 0.3))

        self.cycles_passed = 0

        self.engine.add_drawer(
                draw_char(
                    self.engine,
                    start_vektor=[self.my_drawer_index, 1],
                    char=""
                )
            )




        self.refresh_list = [x*0.1 for x in range(7).__reversed__()]      # [1, 0.9, 0.8, 0.7, 0.6, 0.5]

    def get_new_speed(self):
        return (randrange( 1, 3 )) * 0.2

    def get_new_trail(self):
        return randrange( 10, 20 )

    # selekt a random char from the table
    def getchars(self, start, end): return [chr(i) for i in range(start, end)]

    def refresh(self):
        #process drop
        self.engine.draw_list[self.my_drawer_index].change_parameter(
            char=choice(self.chars),
            color_slot_1=self.col_brite
        )

        #process trail
        if self.cycles_per_raw >= self.cycles_passed:
            self.cycles_passed += 1
        else:
            # save drop position for later
            tmp_drop_y_position = self.engine.draw_list[self.my_drawer_index].start_y

            # fade the trail
            for x in range(1, self.trail_lengh):

                self.engine.draw_list[self.my_drawer_index].start_y -= 1
                if self.engine.draw_list[self.my_drawer_index].start_y < 1:
                    continue
                else:
                    self.engine.draw_list[self.my_drawer_index].change_parameter(
                        char = choice(self.chars),
                        color_slot_1=self.engine.color_conv.rgb_to_color(
                            "f",
                            0,
                            int(116 - (116/self.trail_lengh*x)),
                            int(179 - (179/self.trail_lengh*x)),
                            int(0)
                        )
                    )

            # restore drop position
            self.engine.draw_list[self.my_drawer_index].start_y = tmp_drop_y_position

            self.engine.draw_list[self.my_drawer_index].start_y += 1
            self.cycles_passed = 0



        # when at the end, destreoi drop and generate new one
        if self.engine.draw_list[self.my_drawer_index].start_y > self.engine.canvas_y[1] + self.trail_lengh:
            # reset the drop to top
            self.engine.draw_list[self.my_drawer_index].start_y = self.engine.canvas_y[0]
            self.engine.draw_list[self.my_drawer_index].speed = self.get_new_speed()
            self.engine.draw_list[self.my_drawer_index].trail_lengh = self.get_new_trail()
