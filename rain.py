from e_ascii import grafik_engine
from random import randrange
from rain_drop import rain_drop
from time import sleep
from sys import exit


class rain:

    def __init__(self, return_Q,  kill_chan):

        # init communication Queues
        self.kill_chan =  kill_chan
        self.return_Q = return_Q

        # init ascii engine
        self.engine = grafik_engine()

        self.rain_drop_list = list()
        self.rain_drop_list_start = list()
        self.it_is_raining = True
        self.start_counter = 0

        # add drop to rain list
        for i in range(self.engine.canvas_x[1] + 1):
            # eatch drop hass its owen drawer
            self.rain_drop_list_start.append(rain_drop(self.engine, i))


    def process_rain(self):
        while self.it_is_raining:

            # this part is only for the start
            # add every x loops a drop
            if self.start_counter == 25:
                if len(self.rain_drop_list_start) == 0:
                    pass
                else:
                    tmp_list_index = randrange(len(self.rain_drop_list_start))
                    self.rain_drop_list.append(self.rain_drop_list_start.pop(tmp_list_index))
                    self.start_counter = 0
            else:
                self.start_counter += 1



            # process rain
            for drop in self.rain_drop_list:
                drop.refresh()


            # Return the framebuffer
            # to the main thread
            self.return_Q.put(self.engine.FB)


            # "HANDBREAKE"
            while self.return_Q.qsize() > 5:
                if not self.kill_chan.empty():
                    self.it_is_raining = False
                    exit()
                sleep(0.005)


def let_it_rain(return_Q,  kill_chan):
    rain_obj = rain(return_Q,  kill_chan)
    rain_obj.process_rain()
